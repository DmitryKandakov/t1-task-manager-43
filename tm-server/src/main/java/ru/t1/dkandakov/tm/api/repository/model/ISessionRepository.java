package ru.t1.dkandakov.tm.api.repository.model;

import ru.t1.dkandakov.tm.model.Session;

public interface ISessionRepository extends IUserOwnerRepository<Session> {
}
