package ru.t1.dkandakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.repository.dto.IUserOwnerRepositoryDTO;
import ru.t1.dkandakov.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnerDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractDtoRepository<M> implements IUserOwnerRepositoryDTO<M> {

    protected AbstractUserOwnerDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.merge(model);
    }

}
