package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.ISessionServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.IUserServiceDTO;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectServiceDTO getProjectService();

    @NotNull
    ITaskServiceDTO getTaskService();

    @NotNull
    IUserServiceDTO getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionServiceDTO getSessionService();

}