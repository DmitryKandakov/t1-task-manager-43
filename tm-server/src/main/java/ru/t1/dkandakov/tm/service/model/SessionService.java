package ru.t1.dkandakov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.repository.model.ISessionRepository;
import ru.t1.dkandakov.tm.api.service.IConnectionService;
import ru.t1.dkandakov.tm.api.service.model.ISessionService;
import ru.t1.dkandakov.tm.model.Session;
import ru.t1.dkandakov.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}
