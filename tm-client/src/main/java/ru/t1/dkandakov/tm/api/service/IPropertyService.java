package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull Integer getPasswordIteration();

}
